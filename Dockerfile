FROM node:14 AS build

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install app dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Build the app
RUN npm run build

# Use the official Nginx runtime as the production stage
FROM nginx:latest

# Copy the built files from the build stage
COPY --from=build /usr/src/app/build /usr/share/nginx/html

# Start nginx
CMD ["nginx", "-g", "daemon off;"]
